package com.kalif.katalogmuvi.dictionarys.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.kalif.katalogmuvi.dictionarys.R;


public class SharedPref {
       private static Context context;
    private static SharedPreferences prefs;

    public SharedPref(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    public static void setFirstRun(Boolean input) {
        SharedPreferences.Editor editor = prefs.edit();
        String key = context.getResources().getString(R.string.apps_first_running);
        editor.putBoolean(key, input);
        editor.commit();
    }

    public static Boolean getFirstRun() {
        String key = context.getResources().getString(R.string.apps_first_running);
        return prefs.getBoolean(key, true);
    }
}
