package com.kalif.katalogmuvi.dictionarys.Helper.dbase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.kalif.katalogmuvi.dictionarys.Helper.Config;
import com.kalif.katalogmuvi.dictionarys.Model.KamusModel;

import java.util.ArrayList;

public class KamusDataHelper {
    private static String English = Config.DATABASE_TABLE_ENGLISH;
    private static String Indonesia = Config.DATABASE_TABLE_INDONESIA;

    private Context context;
    private dbhelper dbHelper;
    private SQLiteDatabase database;


    public KamusDataHelper(Context context) {
        this.context = context;
    }

    public KamusDataHelper open() throws SQLException{
        dbHelper = new dbhelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {dbHelper.close();}

    public Cursor searchQueryByName(String query, boolean english){

        String DATABASE_TABLE = english ? English : Indonesia;
        return database.rawQuery("SELECT * FROM " + DATABASE_TABLE +
                " WHERE " + Config.DATABASE_FIELD_WORD + " LIKE '%" + query.trim() + "%'", null);
    }

    public ArrayList<KamusModel> getDataByName(String search, boolean english) {
        KamusModel kamusDataModel;

        ArrayList<KamusModel> arrayList = new ArrayList<>();
        Cursor cursor = searchQueryByName(search, english);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            do {
                kamusDataModel = new KamusModel();
                kamusDataModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_ID)));
                kamusDataModel.setWord(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_WORD)));
                kamusDataModel.setTranslate(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_TRANSLATION)));
                arrayList.add(kamusDataModel);

                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public Cursor queryAllData(boolean english) {
        String DATABASE_TABLE = english ? English : Indonesia;
        return database.rawQuery("SELECT * FROM " + DATABASE_TABLE + " ORDER BY " + Config.DATABASE_FIELD_ID + " ASC", null);
    }

    public ArrayList<KamusModel> getDataALl(boolean english) {
        KamusModel kamusDataModel;

        ArrayList<KamusModel> arrayList = new ArrayList<>();
        Cursor cursor = queryAllData(english);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            do {
                kamusDataModel = new KamusModel();
                kamusDataModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_ID)));
                kamusDataModel.setWord(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_WORD)));
                kamusDataModel.setTranslate(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_TRANSLATION)));
                arrayList.add(kamusDataModel);

                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }


    public long insert(KamusModel kamusDataModel, boolean english) {
        String DATABASE_TABLE = english ? English : Indonesia;
        ContentValues initialValues = new ContentValues();
        initialValues.put(Config.DATABASE_FIELD_WORD, kamusDataModel.getWord());
        initialValues.put(Config.DATABASE_FIELD_TRANSLATION, kamusDataModel.getTranslate());
        return database.insert(DATABASE_TABLE, null, initialValues);
    }

    public void insertTransaction(ArrayList<KamusModel> kamusDataModels, boolean english) {
        String DATABASE_TABLE = english ? English : Indonesia;
        String sql = "INSERT INTO " + DATABASE_TABLE + " (" +
                Config.DATABASE_FIELD_WORD + ", " +
                Config.DATABASE_FIELD_TRANSLATION + ") VALUES (?, ?)";

        database.beginTransaction();

        SQLiteStatement stmt = database.compileStatement(sql);
        for (int i = 0; i < kamusDataModels.size(); i++) {
            stmt.bindString(1, kamusDataModels.get(i).getWord());
            stmt.bindString(2, kamusDataModels.get(i).getTranslate());
            stmt.execute();
            stmt.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public void update(KamusModel kamusDataModel, boolean english) {
        String DATABASE_TABLE = english ? English : Indonesia;
        ContentValues args = new ContentValues();
        args.put(Config.DATABASE_FIELD_WORD, kamusDataModel.getWord());
        args.put(Config.DATABASE_FIELD_TRANSLATION, kamusDataModel.getTranslate());
        database.update(DATABASE_TABLE, args, Config.DATABASE_FIELD_ID+ "= '" + kamusDataModel.getId() + "'", null);
    }

    public void delete(int id, boolean english) {
        String DATABASE_TABLE = english ? English : Indonesia;
        database.delete(DATABASE_TABLE, Config.DATABASE_FIELD_ID + " = '" + id + "'", null);
    }

}