package com.kalif.katalogmuvi.dictionarys.Fragment;


import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kalif.katalogmuvi.dictionarys.Adapter.KamusAdapter;
import com.kalif.katalogmuvi.dictionarys.Helper.dbase.KamusDataHelper;
import com.kalif.katalogmuvi.dictionarys.Model.KamusModel;
import com.kalif.katalogmuvi.dictionarys.R;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Ind_EnFragment extends Fragment {

    private MaterialSearchBar search;
    private RecyclerView rv;

    private KamusDataHelper kamusDataHelper;
    private KamusAdapter kamusDataAdapter;

    private ArrayList<KamusModel> kamusDataModels;

    private boolean English = false;

    public Ind_EnFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ind__en, container, false);
        initView(view);
        kamusDataModels = new ArrayList<>();
        kamusDataHelper = new KamusDataHelper(getActivity());
        kamusDataAdapter = new KamusAdapter(kamusDataModels, getActivity());
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setAdapter(kamusDataAdapter);

        search.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                kamusDataAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        loadData("");

        return (view);
    }


    private void loadData(String search) {
        try {
            kamusDataHelper.open();
            kamusDataModels = kamusDataHelper.getDataALl(English);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            kamusDataHelper.close();
        }
        kamusDataAdapter.replaceAll(kamusDataModels);
    }

    private void initView(View view) {
        search = view.findViewById(R.id.search);
        rv =  view.findViewById(R.id.rv);
    }

}
